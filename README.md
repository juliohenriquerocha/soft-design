# Sicredi - Pauta REST API

Aplicação backend responsável por gerenciar as Pautas que serão votadas pelos Cooperados.
Por meio dessa é possível inserir uma nova Pauta, listar todos as Pautas, abrir sessão, votar na Pauta e contabilizar os resultados.

# Guia de instalação

## O que você irá precisar

- [Ubuntu 16.04](https://releases.ubuntu.com/16.04/) ou distribuição compatível;
- [JDK 1.8](https://www.oracle.com/java/technologies/javase-downloads.html) ou superior;
- [Maven 3.2](https://maven.apache.org/download.cgi) ou superior;
- [Docker 19.03.6](https://docs.docker.com/) ou superior;
- [Docker Compose](https://docs.docker.com/compose/install/);
- [Postman](https://www.postman.com/downloads/) ou similar;
- Editor de texto de sua preferência;

## O que é preciso fazer

1. Edite o arquivo `docker-compose.yml#18` para inserir o caminho de persistência dos dados do PostgreSQL.
   Assegure que a pasta permite a escrita/leitura dos dados;

2. Para iniciar a aplicação:

2.1. Execute o seguinte comando, no seu terminal linux, para tornar o arquivo executável:

```sh
$ sudo chmod +x startup.sh
```

2.2. Execute o arquivo no seu terminal linux:

```sh
$ ./startup.sh
```

3. Pronto. A aplicação está executando. Utilize o Postman ou similar para fazer uso da API. Aplicação estará rodando na seguinte url:

```
http://localhost:8080/soft-design/

```

## Documentação da API:

[Link para a documentação da API](https://documenter.getpostman.com/view/3579934/TVev44uW)

## Suite de teste

Para executar a suite de teste produzidas, rode o seguinte comando:

```sh
$ sudo chmod +x test.sh
```

E depois

```sh
$ ./test.sh
```

> `Os registros presente no banco de dados serão eliminados antes e depois da execução de todos os testes!`
