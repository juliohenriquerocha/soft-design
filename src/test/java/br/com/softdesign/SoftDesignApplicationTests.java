package br.com.softdesign;

import br.com.softdesign.enums.ResultadoPautaEnum;
import br.com.softdesign.enums.VotoEnum;
import br.com.softdesign.exception.SoftDesignBusinessException;
import br.com.softdesign.exception.SoftDesignPersistenceException;
import br.com.softdesign.model.Pauta;
import br.com.softdesign.model.Voto;
import br.com.softdesign.service.PautaService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.hamcrest.Matchers;
import org.hamcrest.core.IsNull;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class SoftDesignApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private PautaService service;

	private List<Pauta> pautaFactory(int size){
		List<Pauta> result = new LinkedList<>();
		for (int i = 0; i < size; i++) {
			Pauta p = new Pauta();
			p.setTitulo("Titulo Pauta" + i);
			result.add(p);
		}
		return result;
	}

    private List<Voto> votosFactory(int size, Pauta pauta){
        List<Voto> result = new LinkedList<>();
        for (int i = 0; i < size; i++) {
            Voto v = new Voto();
            v.setIdCooperado(i+1L);
            v.setPauta(pauta);
            v.setDataVoto(new Date());
            v.setVoto(i % 2 == 0 ? VotoEnum.SIM : VotoEnum.NAO);
            result.add(v);
        }
        return result;
    }


	// Inserindo Pauta
	@Test
	void insertPautaTest1() throws Exception {

		List<Pauta> list = pautaFactory(2);

		Pauta p1 = list.get(0);
		mockMvc.perform(post("/pauta")
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(p1)))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.titulo").value(p1.getTitulo()))
                .andExpect(jsonPath("$.inicioSessao").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.finalSessao").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.resultado").value(IsNull.nullValue()))
				.andExpect(jsonPath("$.votos").value(Matchers.containsInAnyOrder(new Voto[0])));

        Pauta p2 = list.get(0);
        mockMvc.perform(post("/pauta")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(p2)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.titulo").value(p2.getTitulo()))
                .andExpect(jsonPath("$.inicioSessao").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.finalSessao").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.resultado").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.votos").value(Matchers.containsInAnyOrder(new Voto[0])));

	}

    // Editar Pauta
    @Test
    void editarPautaTest1() throws Exception {

        List<Pauta> list = pautaFactory(1);
        Pauta p1 = list.get(0);
        service.save(p1);

        p1.setTitulo("Novo título");

        mockMvc.perform(put("/pauta")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(p1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.titulo").value("Novo título"))
                .andExpect(jsonPath("$.inicioSessao").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.finalSessao").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.resultado").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.votos").value(Matchers.containsInAnyOrder(new Voto[0])));



    }

    // Editar Pauta com sessão já iniciada
    @Test
    void editarPautaComSessaoIniciadaTest1() throws Exception {

        List<Pauta> list = pautaFactory(1);
        Pauta p1 = list.get(0);
        service.save(p1);

        //iniciar sessão de votação
        mockMvc.perform(post("/pauta/" + p1.getId() + "/abrir-sessao")
                .contentType("application/json"))
                .andExpect(status().isOk());

        p1.setTitulo("Novo título");

        mockMvc.perform(put("/pauta")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(p1)))
                .andExpect(status().isNotAcceptable());

    }

	//NOT NULL constraints
	@Test
	void InsertPautaEntradaInvalidaTest2() throws Exception {

		List<Pauta> list = pautaFactory(1);

		Pauta p1 = list.get(0);
		p1.setTitulo(null);
		mockMvc.perform(post("/pauta")
				.contentType("application/json")
				.content(objectMapper.writeValueAsString(p1)))
				.andExpect(status().isBadRequest());


	}

    // POST /pauta
    //Inserindo Pauta já com dados contabilizados
    @Test
    void InsertPautaJaComDadosTest3() throws Exception {

        List<Pauta> list = pautaFactory(1);

        Pauta p1 = list.get(0);

        p1.setResultado(ResultadoPautaEnum.APROVADA);
        p1.setSimContabilizado(10L);
        p1.setNaoContabilizado(5L);
        p1.setInicioSessao(new Date());
        p1.setFinalSessao(new Date());
        p1.setVotos(votosFactory(10, p1));

        mockMvc.perform(post("/pauta")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(p1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.titulo").value(p1.getTitulo()))
                .andExpect(jsonPath("$.inicioSessao").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.finalSessao").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.resultado").value(IsNull.nullValue()))
                .andExpect(jsonPath("$.votos").value(Matchers.containsInAnyOrder(new Voto[0])));


    }

    	// GET /pauta
	@Test
	void listAllPautaTest1() throws Exception {

		List<Pauta> list = pautaFactory(5);
		for (Pauta w: list) {
			service.save(w);
		}

		mockMvc.perform(get("/pauta")
				.contentType("application/json"))
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.content().string(objectMapper.writeValueAsString(list)));
	}

	// GET /pauta
	//Empty list
	@Test
	void listAllSemPautaTest2() throws Exception {
		mockMvc.perform(get("/pauta")
				.contentType("application/json"))
				.andExpect(status().isOk())
				.andExpect(MockMvcResultMatchers.content().string(objectMapper.writeValueAsString(new ArrayList<>())));
	}


	@Test
	void abrirSessaoTest1() throws Exception {

		List<Pauta> list = pautaFactory(2);
		Pauta w0 = list.get(0);
		service.save(w0);

		mockMvc.perform(post("/pauta/" + w0.getId() + "/abrir-sessao").param("minutos", "2")
				.contentType("application/json"))
				.andExpect(status().isOk());

		Pauta pautaAtualizada = service.findById(w0.getId());
		Assertions.assertThat(getDateDiffInMinutes(pautaAtualizada.getInicioSessao(), pautaAtualizada.getFinalSessao())).isEqualTo(2);


        Pauta w1 = list.get(1);
        service.save(w1);

        mockMvc.perform(post("/pauta/" + w1.getId() + "/abrir-sessao")
                .contentType("application/json"))
                .andExpect(status().isOk());

        pautaAtualizada = service.findById(w1.getId());
        Assertions.assertThat(getDateDiffInMinutes(pautaAtualizada.getInicioSessao(), pautaAtualizada.getFinalSessao())).isEqualTo(1);

	}

	//0 minutos ou tempo negativo
    @Test
    void abrirSessaoEntradaInvalidaTest2() throws Exception {

        List<Pauta> list = pautaFactory(1);
        Pauta w0 = list.get(0);
        service.save(w0);

        mockMvc.perform(post("/pauta/" + w0.getId() + "/abrir-sessao").param("minutos", "0")
                .contentType("application/json"))
                .andExpect(status().isNotAcceptable());

        mockMvc.perform(post("/pauta/" + w0.getId() + "/abrir-sessao").param("minutos", "-2")
                .contentType("application/json"))
                .andExpect(status().isNotAcceptable());


    }

    //abrir sessão já aberta ou já fechada
    @Test
    void abrirSessaoJaAbertaOuFechadaTest3() throws Exception {

        List<Pauta> list = pautaFactory(1);
        Pauta w0 = list.get(0);
        service.save(w0);

        mockMvc.perform(post("/pauta/" + w0.getId() + "/abrir-sessao")
                .contentType("application/json"))
                .andExpect(status().isOk());

        // sessão já aberta
        mockMvc.perform(post("/pauta/" + w0.getId() + "/abrir-sessao").param("minutos", "2")
                .contentType("application/json"))
                .andExpect(status().isNotAcceptable());

        Thread.sleep(60000); // esperando um minito para terminar a sessão

        // sessão já fechada
        mockMvc.perform(post("/pauta/" + w0.getId() + "/abrir-sessao").param("minutos", "2")
                .contentType("application/json"))
                .andExpect(status().isNotAcceptable());


    }


    //votar
    @Test
    void votarTest1() throws Exception {

        List<Pauta> list = pautaFactory(1);
        Pauta w0 = list.get(0);
        service.save(w0);

        //abrindo sessão para permitir votar
        mockMvc.perform(post("/pauta/" + w0.getId() + "/abrir-sessao")
                .contentType("application/json"))
                .andExpect(status().isOk());

        List<Voto> votos = votosFactory(4, w0);

        for (Voto v : votos){
            mockMvc.perform(post("/pauta/" + w0.getId() + "/votar").param("cooperado", v.getIdCooperado().toString())
                    .param("voto", v.getVoto() == VotoEnum.SIM ? "Sim" : "Não")
                    .contentType("application/json"))
                    .andExpect(status().isOk());
        }


    }

    //fora da sessão ou com dados errados
    @Test
    void votarForaSessaoOuEntradaInvalidaTest2() throws Exception {

        List<Pauta> list = pautaFactory(1);
        Pauta w0 = list.get(0);
        service.save(w0);

        //Com sessão fechada
        List<Voto> votos = votosFactory(4, w0);
        for (Voto v : votos){
            mockMvc.perform(post("/pauta/" + w0.getId() + "/votar").param("cooperado", v.getIdCooperado().toString())
                    .param("voto", v.getVoto() == VotoEnum.SIM ? "Sim" : "Não")
                    .contentType("application/json"))
                    .andExpect(status().isNotAcceptable());
        }


        //abrindo sessão para permitir votar
        mockMvc.perform(post("/pauta/" + w0.getId() + "/abrir-sessao")
                .contentType("application/json"))
                .andExpect(status().isOk());

        //sem id do cooperado
        votos = votosFactory(4, w0);
        for (Voto v : votos){
            mockMvc.perform(post("/pauta/" + w0.getId() + "/votar") //sem id do cooperado
                    .param("voto", "qualquer")
                    .contentType("application/json"))
                    .andExpect(status().isBadRequest());
        }

        //com voto errado
        votos = votosFactory(4, w0);
        for (Voto v : votos){
            mockMvc.perform(post("/pauta/" + w0.getId() + "/votar").param("cooperado", v.getIdCooperado().toString())
                    .param("voto", "qualquer")
                    .contentType("application/json"))
                    .andExpect(status().isNotAcceptable());
        }

    }


    //voto duplicado
    @Test
    void votarDuplicadoTest3() throws Exception {

        List<Pauta> list = pautaFactory(1);
        Pauta w0 = list.get(0);
        service.save(w0);

        //abrindo sessão para permitir votar
        mockMvc.perform(post("/pauta/" + w0.getId() + "/abrir-sessao")
                .contentType("application/json"))
                .andExpect(status().isOk());

        List<Voto> votos = votosFactory(3, w0);

        //o primeiro voto deve ser permitido
        mockMvc.perform(post("/pauta/" + w0.getId() + "/votar").param("cooperado", votos.get(0).getIdCooperado().toString())
                .param("voto", votos.get(0).getVoto() == VotoEnum.SIM ? "Sim" : "Não")
                .contentType("application/json"))
                .andExpect(status().isOk());

        //Os demais não
        for (Voto v : votos){
            v.setIdCooperado(votos.get(0).getIdCooperado()); //mesmo cooperado tentando votar várias vezes
            mockMvc.perform(post("/pauta/" + w0.getId() + "/votar").param("cooperado", v.getIdCooperado().toString())
                    .param("voto", v.getVoto() == VotoEnum.SIM ? "Sim" : "Não")
                    .contentType("application/json"))
                    .andExpect(status().isNotAcceptable());
        }

    }

    //voto com sessão encerrada
    @Test
    void votarSessaoEncerradaTest4() throws Exception {

        List<Pauta> list = pautaFactory(1);
        Pauta w0 = list.get(0);
        service.save(w0);

        //abrindo sessão para permitir votar
        mockMvc.perform(post("/pauta/" + w0.getId() + "/abrir-sessao")
                .contentType("application/json"))
                .andExpect(status().isOk());

        // esperando um minito para terminar a sessão
        Thread.sleep(60000);

        List<Voto> votos = votosFactory(1, w0);
        Voto v = votos.get(0);
        //o voto não deve ser permitido pois a sessão já foi encerrada
        mockMvc.perform(post("/pauta/" + w0.getId() + "/votar").param("cooperado", votos.get(0).getIdCooperado().toString())
                .param("voto", votos.get(0).getVoto() == VotoEnum.SIM ? "Sim" : "Não")
                .contentType("application/json"))
                .andExpect(status().isNotAcceptable());


    }

    //contabilizar sessão
    @Test
    void contabilizarResultadoTest1() throws Exception {

        List<Pauta> list = pautaFactory(1);
        Pauta w0 = list.get(0);
        service.save(w0);

        //abrindo sessão para permitir votar
        mockMvc.perform(post("/pauta/" + w0.getId() + "/abrir-sessao")
                .contentType("application/json"))
                .andExpect(status().isOk());

        List<Voto> votos = votosFactory(10, w0);

        for (Voto v : votos){
            mockMvc.perform(post("/pauta/" + w0.getId() + "/votar").param("cooperado", v.getIdCooperado().toString())
                    .param("voto", v.getVoto() == VotoEnum.SIM ? "Sim" : "Não")
                    .contentType("application/json"))
                    .andExpect(status().isOk());
        }

        // esperando um minito para terminar a sessão
        Thread.sleep(60000);

        mockMvc.perform(get("/pauta/" + w0.getId() + "/contabilizar-resultado")
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.titulo").value(w0.getTitulo()))
                .andExpect(jsonPath("$.resultado").value("REJEITADA"))
                .andExpect(jsonPath("$.simContabilizado").value(5))
                .andExpect(jsonPath("$.naoContabilizado").value(5));


    }

    //contabilizar sessão
    @Test
    void contabilizarResultadoTest2() throws Exception {

        List<Pauta> list = pautaFactory(1);
        Pauta w0 = list.get(0);
        service.save(w0);

        //abrindo sessão para permitir votar
        mockMvc.perform(post("/pauta/" + w0.getId() + "/abrir-sessao")
                .contentType("application/json"))
                .andExpect(status().isOk());

        List<Voto> votos = votosFactory(5, w0);

        for (Voto v : votos){
            v.setVoto(VotoEnum.SIM);
            mockMvc.perform(post("/pauta/" + w0.getId() + "/votar").param("cooperado", v.getIdCooperado().toString())
                    .param("voto", v.getVoto() == VotoEnum.SIM ? "Sim" : "Não")
                    .contentType("application/json"))
                    .andExpect(status().isOk());
        }

        // esperando um minito para terminar a sessão
        Thread.sleep(60000);

        mockMvc.perform(get("/pauta/" + w0.getId() + "/contabilizar-resultado")
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.titulo").value(w0.getTitulo()))
                .andExpect(jsonPath("$.resultado").value("APROVADA"))
                .andExpect(jsonPath("$.simContabilizado").value(5))
                .andExpect(jsonPath("$.naoContabilizado").value(0));


    }

    //contabilizar resultado sem sessao aberta
    @Test
    void contabilizarResultadoSemSessaoAbertaTest3() throws Exception {

        List<Pauta> list = pautaFactory(1);
        Pauta w0 = list.get(0);
        service.save(w0);


        mockMvc.perform(get("/pauta/" + w0.getId() + "/contabilizar-resultado")
                .contentType("application/json"))
                .andExpect(status().isNotAcceptable());


    }

    //contabilizar resultado com sessão aberta
    @Test
    void contabilizarResultadoComSessaoAbertaTest4() throws Exception {

        List<Pauta> list = pautaFactory(1);
        Pauta w0 = list.get(0);
        service.save(w0);

        //abrindo sessão para permitir votar
        mockMvc.perform(post("/pauta/" + w0.getId() + "/abrir-sessao")
                .contentType("application/json"))
                .andExpect(status().isOk());

        List<Voto> votos = votosFactory(5, w0);

        for (Voto v : votos){
            mockMvc.perform(post("/pauta/" + w0.getId() + "/votar").param("cooperado", v.getIdCooperado().toString())
                    .param("voto", v.getVoto() == VotoEnum.SIM ? "Sim" : "Não")
                    .contentType("application/json"))
                    .andExpect(status().isOk());
        }


        mockMvc.perform(get("/pauta/" + w0.getId() + "/contabilizar-resultado")
                .contentType("application/json"))
                .andExpect(status().isNotAcceptable());


    }

    //<EXTRA>

    //consultar status dos cooperados
    @Test
    void statusCooperadoExternoTest1() throws Exception {

        MvcResult result = mockMvc.perform(get("/extra/status-cooperado/21864975067")
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        String content = result.getResponse().getContentAsString();

        Assertions.assertThat(content).isIn("ABLE_TO_VOTE", "UNABLE_TO_VOTE");

        result = mockMvc.perform(get("/extra/status-cooperado/73749735000")
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andReturn();

        content = result.getResponse().getContentAsString();

        Assertions.assertThat(content).isIn("ABLE_TO_VOTE", "UNABLE_TO_VOTE");

    }

    @Test
    void statusCooperadoExternoCpfInvalidoTest1() throws Exception {

        mockMvc.perform(get("/extra/status-cooperado/12345678909")
                .contentType("application/json"))
                .andExpect(status().isNotFound());

        mockMvc.perform(get("/extra/status-cooperado/12345")
                .contentType("application/json"))
                .andExpect(status().isNotFound());

        mockMvc.perform(get("/extra/status-cooperado/1234567890123456789")
                .contentType("application/json"))
                .andExpect(status().isNotFound());


    }

    //</EXTRA>




	@BeforeEach
	@AfterEach
	void cleanDataBase() throws SoftDesignBusinessException, SoftDesignPersistenceException {
		final Iterable<Pauta> iterator = service.findAll();
		List<Pauta> list = new ArrayList<Pauta>();
		iterator.forEach(list::add);

		service.batchDelete(list);
	}

    long getDateDiffInMinutes(Date date1, Date date2) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return diffInMillies / (60 * 1000);
    }
}


