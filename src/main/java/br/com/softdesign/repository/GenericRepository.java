package br.com.softdesign.repository;

import br.com.softdesign.exception.SoftDesignPersistenceException;
import br.com.softdesign.model.Identifiable;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Classe abstrata com as operações comuns utilizadas por todas as entidades da aplicação.
 * @author julio
 * @param <T extends Identifiable>
 */
public abstract class GenericRepository<T extends Identifiable> {

    @PersistenceContext
    EntityManager entityManager;

    Class<T> persistentClass;

    @SuppressWarnings("unchecked")
    protected GenericRepository() {
        this.persistentClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /**
     * Método responsável por fazer o fetch dos objetos na base de dados.
     * @return - Iterador com os objetos
     * @throws SoftDesignPersistenceException - Erro no fetch na base de dados.
     */
    public Iterable<T> findAll() throws SoftDesignPersistenceException {
        try {
            TypedQuery<T> query = entityManager.createQuery("FROM " + this.persistentClass.getName(), this.persistentClass);
            List<T> list = query.getResultList();
            return list;
        } catch (Exception e){
            throw new SoftDesignPersistenceException("Os registros não pôderam ser recuperados.", e);
        }
    }

    /**
     * Método responsável por fazer o fetch do objeto na base de dados pelo seu ID.
     * @param id - ID do objeto buscado.
     * @return - o objeto buscado.
     * @throws SoftDesignPersistenceException - Erro no momento do fetch na base de dados.
     */
    public T findById(Long id) throws SoftDesignPersistenceException {
        try {
            T result = entityManager.find(this.persistentClass, id);
            if(result == null){
                throw new SoftDesignPersistenceException("Nenhum registro encontrado com o identificador informado.");
            }
            return result;
        } catch (Exception e){
          throw new SoftDesignPersistenceException("O registro não pôde ser recuperado.", e);
        }
    }

    /**
     * Método responsável por realizar a persistência de um objeto na base de dados.
     * @param object - objeto a ser persistido.
     * @throws SoftDesignPersistenceException - Erro no momento da inserção ou atualização na base de dados.
     */
    @Transactional(rollbackFor = SoftDesignPersistenceException.class)
    public void save(T object) throws SoftDesignPersistenceException {
        try {
            checkBeforeSave(object);
            if (object.getId() == null){
                entityManager.persist(object);
            } else {
                entityManager.merge(object);
            }
        } catch (Exception e){
            throw new SoftDesignPersistenceException("O registro não pôde ser salvo.", e);
        }
    }

    /**
     * Método responsável por remover um objeto da base de dados.
     * @param id - id do objeto a ser removido
     * @throws SoftDesignPersistenceException  - Erro no momento da remoção na base de dados.
     */
    @Transactional
    public void delete(Long id) throws SoftDesignPersistenceException {
        try {
            T o = findById(id);
            entityManager.remove(o);
        } catch (Exception e){
            throw new SoftDesignPersistenceException("Não foi possível remover o objeto. ", e);
        }
    }


    protected void checkBeforeSave(T object){

    }

    protected void checkBeforeDelete(T object){

    }




}