package br.com.softdesign.repository;

import br.com.softdesign.model.Pauta;
import org.springframework.stereotype.Repository;

@Repository
public class PautaRepository extends GenericRepository<Pauta> {

}