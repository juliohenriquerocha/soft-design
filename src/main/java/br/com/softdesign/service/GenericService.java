package br.com.softdesign.service;

import br.com.softdesign.exception.SoftDesignBusinessException;
import br.com.softdesign.exception.SoftDesignPersistenceException;
import br.com.softdesign.model.Identifiable;
import br.com.softdesign.repository.GenericRepository;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Classe abstrata com operações comuns de serviços para todas as entidades manipuladas na aplicação.
 * @author julio
 * @param <T exntend Identifiable>
 */
@Service
public abstract class GenericService<T extends Identifiable> {

    public abstract GenericRepository<T> getRepository();

    /**
     * Método responsável por realizar a busca de todos os objetos a nível de service.
     * @return - Iterador com os objetos
     * @throws SoftDesignPersistenceException - Erro ocorrido no acesso dos objetos.
     */
    public Iterable<T> findAll() throws SoftDesignPersistenceException {
        return getRepository().findAll();
    }

    /**
     * Método responsável por realizar a busca de um objeto pelo ID a nível de service.
     * @param id - Id do objeto buscado.
     * @return o objeto buscado
     * @throws SoftDesignPersistenceException - Erro ocorrido no acesso do objeto.
     */
    public T findById(Long id) throws SoftDesignPersistenceException {
        return getRepository().findById(id);
    }

    /**
     * Método responsável por realizar a persistência de um objeto a nível de service.
     * @param object - objeto a ser persistido.
     * @throws SoftDesignPersistenceException - Erro no momento da persistência do objeto.
     */
    public void save(T object) throws SoftDesignPersistenceException, SoftDesignBusinessException {
        getRepository().save(object);
    }

    /**
     * Método responsável por remover um conjunto de objetos a nível de serviço.
     * @param list - lista dos objetos a serem removidos.
     * @throws SoftDesignPersistenceException - Erro no momento da remoção dos objetos
     */
    public void batchDelete(List<T> list) throws SoftDesignPersistenceException {
        for (T object: list) {
            getRepository().delete(object.getId());
        }
    }


}
