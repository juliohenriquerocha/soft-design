package br.com.softdesign.service;

import br.com.softdesign.enums.ResultadoPautaEnum;
import br.com.softdesign.enums.VotoEnum;
import br.com.softdesign.exception.SoftDesignBusinessException;
import br.com.softdesign.exception.SoftDesignPersistenceException;
import br.com.softdesign.message.MessageSender;
import br.com.softdesign.model.Pauta;
import br.com.softdesign.model.Voto;
import br.com.softdesign.repository.PautaRepository;
import org.apache.logging.log4j.util.Strings;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

@Service
public class PautaService extends GenericService<Pauta> {

	@Autowired
	private PautaRepository pautaRepository;

	@Autowired
	private MessageSender sender;

	@Override
	public PautaRepository getRepository() {
		return pautaRepository;
	}

	/**
	 * Serviço responsável por solicitar a persistência de uma Pauta
	 * @param object - objeto a ser persistido.
	 * @throws SoftDesignPersistenceException - Erro no momento da persistência.
	 */
	@Override
	public void save(Pauta object) throws SoftDesignPersistenceException, SoftDesignBusinessException {
		//Se for um novo objeto, deve ser setado a data de criação
		if(object.getId() == null){
			object.setDataCriacao(new Date());
		} else{
			Pauta pauta = getRepository().findById(object.getId());
			if(pauta != null && pauta.getInicioSessao() != null){
				throw new SoftDesignBusinessException("A Pauta não pode ser editada pois a sessão já foi iniciada.");
			}
		}
		//A edição só será permitida caso a sessão não tenha sido aberta
		super.save(object);
	}

	/**
	 * Serviço responsável por realizar a abertura de uma sessão para votação.
	 * @param idPauta - id da Pauta que terá sua sessão aberta.
	 * @param minutos - quantidade de minutos que a sessão ficará aberta.
	 * @throws SoftDesignBusinessException - Alguma regra de negócio não foi respeitada.
	 * @throws SoftDesignPersistenceException - Erro no momento da coleta ou persistência dos dados.
	 */
	public void openSession(Long idPauta, Integer minutos) throws SoftDesignBusinessException, SoftDesignPersistenceException {
		//o time deve ser superior a 1
		if(minutos == null || minutos < 1) {
			throw new SoftDesignBusinessException("O tempo de sessão deve ser superior a 1 minuto.");
		}
		Pauta pauta = getRepository().findById(idPauta);

		//se a sessão já tiver ocorrido, não deve ser permitido iniciar novamente
		if(pauta.getInicioSessao() != null){
			throw new SoftDesignBusinessException("A sessão da pauta já foi iniciada.");
		}

		Date agora = new Date();

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(agora);
		calendar.add(Calendar.MINUTE, minutos);

		pauta.setInicioSessao(agora);
		pauta.setFinalSessao(calendar.getTime());
		getRepository().save(pauta);

		try{
			//Notificar na mensageria do término da sessão
			pautaFinalMessageNotify(pauta);
		}catch (Exception e){
			//o anúncio via mensageria não deve ser um impeditivo.
		}

	}

	/**
	 * Método privado responsável por realizar a notificação do término da Pauta.
	 * @param pauta - Pauta que será anunciada.
	 */
	private void pautaFinalMessageNotify(Pauta pauta){
		Long delay = pauta.getFinalSessao().getTime() - pauta.getInicioSessao().getTime();
		sender.sendMessage(String.format("A Pauta com o título \"%s\" teve sua sessão de votação encerrada.", pauta.getTitulo()), delay);
	}

	/**
	 * Serviço responsável por contabilizar o novo voto em uma Pauta.
	 * @param idPauta - id da Pauta que receberá o voto
	 * @param idCooperado - id do Cooperado que está votando.
	 * @param voto - voto do Cooperado na Pauta ["Sim" , "Não"]
	 * @throws SoftDesignBusinessException - Caso alguma regra de negócio não seja respeitada.
	 * @throws SoftDesignPersistenceException - Caso ocorra algum erro na coleta ou persistência dos dados.
	 */
	public void vote(Long idPauta, Long idCooperado, String voto) throws SoftDesignBusinessException, SoftDesignPersistenceException {
		Pauta pauta = getRepository().findById(idPauta);
		Date agora = new Date();
		if(pauta.getInicioSessao() == null || pauta.getInicioSessao().after(agora) || pauta.getFinalSessao().before(agora)){
			throw new SoftDesignBusinessException("A sessão não está aberta para votação.");
		}

		//apenas votos no formato SIM e NÃO são permitidos
		if(Strings.isNotBlank(voto) && !Arrays.asList("sim", "não").contains(voto.toLowerCase())) {
			throw new SoftDesignBusinessException("Apenas os votos \"SIM\" ou \"NÃO\" são aceitos.");
		}

		Voto votoObj = new Voto();
		votoObj.setPauta(pauta);
		votoObj.setIdCooperado(idCooperado);
		votoObj.setDataVoto(new Date());
		votoObj.setVoto(voto.toLowerCase().equals("sim") ? VotoEnum.SIM : VotoEnum.NAO);

		pauta.getVotos().add(votoObj);
		try{
			getRepository().save(pauta);
		} catch (Exception e){
			if(e.getCause() instanceof ConstraintViolationException && e.getCause().getCause().getMessage().toLowerCase().contains("uk_voto_pauta_cooperado")){
				throw new SoftDesignBusinessException("Apenas um voto por cooperado é permitido.");
			}
			throw e;
		}

	}

	/**
	 * Serviço responsável por realizar a contabilização dos votos em uma Pauta.
	 * @param idPauta - id da Pauta que terá seus votos contabilizados.
	 * @return - a Pauta que teve seus votos contabilizados.
	 * @throws SoftDesignBusinessException - Caso alguma regra de negócio não seja respeitada.
	 * @throws SoftDesignPersistenceException - Caso ocorra algum erro na coleta ou persistência dos dados.
	 */
	public Pauta countResult(Long idPauta) throws SoftDesignBusinessException, SoftDesignPersistenceException {
		Pauta pauta = getRepository().findById(idPauta);
		Date agora = new Date();

		if(pauta.getInicioSessao() == null){
			throw new SoftDesignBusinessException("A sessão de votação não foi aberta.");
		}

		if(pauta.getFinalSessao().after(agora)){
			throw new SoftDesignBusinessException("A sessão ainda está aberta para votação.");
		}

		//só é necessário contabilizar, se ainda não tiver sido
		if(pauta.getResultado() == null){
			long quantidadeVotosSim = pauta.getVotos().stream().filter(v -> v.getVoto() == VotoEnum.SIM).count();
			long quantidadeVotosNao = pauta.getVotos().stream().filter(v -> v.getVoto() == VotoEnum.NAO).count();

			pauta.setSimContabilizado(quantidadeVotosSim);
			pauta.setNaoContabilizado(quantidadeVotosNao);
			pauta.setResultado(quantidadeVotosSim > quantidadeVotosNao ? ResultadoPautaEnum.APROVADA : ResultadoPautaEnum.REJEITADA); // para a pauta ser aprovada, são necessários 50% dos votos + 1

			getRepository().save(pauta);
		}

		return pauta;
	}


}
