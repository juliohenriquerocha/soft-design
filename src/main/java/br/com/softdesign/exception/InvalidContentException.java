package br.com.softdesign.exception;


public class InvalidContentException extends RuntimeException {

    public InvalidContentException(final String message) {
        super(message);
    }

}
