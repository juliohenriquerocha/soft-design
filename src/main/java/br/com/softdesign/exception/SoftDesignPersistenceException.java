package br.com.softdesign.exception;

public class SoftDesignPersistenceException extends Exception {

	private static final long	serialVersionUID	= 1497217279149565659L;

	public SoftDesignPersistenceException(String mensagem) {
		super(mensagem);
	}

	public SoftDesignPersistenceException(String mensagem, Throwable throwable) {
		super(mensagem, throwable);
	}

}
