package br.com.softdesign.exception;

public class SoftDesignBusinessException extends Exception {

	private static final long	serialVersionUID	= 1497217279149565659L;

	public SoftDesignBusinessException(String mensagem) {
		super(mensagem);
	}

	public SoftDesignBusinessException(String mensagem, Throwable throwable) {
		super(mensagem, throwable);
	}

}
