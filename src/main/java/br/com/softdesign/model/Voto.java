package br.com.softdesign.model;

import br.com.softdesign.enums.ResultadoPautaEnum;
import br.com.softdesign.enums.VotoEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "TB_VOTO",   uniqueConstraints = {@UniqueConstraint(columnNames = {"ID_PAUTA", "ID_COOPERADO"}, name = "UK_VOTO_PAUTA_COOPERADO")})
public class Voto implements Identifiable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @NotNull(message = "A Pauta deve ser informada")
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "ID_PAUTA", referencedColumnName = "ID", insertable = true, updatable = false, nullable = false)
    private Pauta pauta;

    @NotNull(message = "O id do Cooperado deve ser informado")
    @Column(name = "ID_COOPERADO", nullable = false, updatable = false)
    private Long idCooperado;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TIMESTAMP_VOTO", nullable = false)
    private Date dataVoto;

    @NotNull(message = "O valor do voto deve ser informado")
    @Column(name = "VOTO", length = 3, nullable = false)
    @Enumerated(value = EnumType.STRING)
    private VotoEnum voto;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Pauta getPauta() {
        return pauta;
    }

    public void setPauta(Pauta pauta) {
        this.pauta = pauta;
    }

    public Long getIdCooperado() {
        return idCooperado;
    }

    public void setIdCooperado(Long idCooperado) {
        this.idCooperado = idCooperado;
    }

    public Date getDataVoto() {
        return dataVoto;
    }

    public void setDataVoto(Date dataVoto) {
        this.dataVoto = dataVoto;
    }

    public VotoEnum getVoto() {
        return voto;
    }

    public void setVoto(VotoEnum voto) {
        this.voto = voto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Voto)) return false;
        Voto voto1 = (Voto) o;
        return getId().equals(voto1.getId()) &&
                getPauta().equals(voto1.getPauta()) &&
                getIdCooperado().equals(voto1.getIdCooperado()) &&
                getDataVoto().equals(voto1.getDataVoto()) &&
                getVoto().equals(voto1.getVoto());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getPauta(), getIdCooperado(), getDataVoto(), getVoto());
    }

    @Override
    public String toString() {
        return "Voto{" +
                "id=" + id +
                ", pauta=" + pauta +
                ", idCooperado=" + idCooperado +
                ", dataVoto=" + dataVoto +
                ", voto=" + voto +
                '}';
    }
}
