package br.com.softdesign.model;

import java.io.Serializable;
import java.util.UUID;

public interface Identifiable extends Serializable {

	public Long getId();
	public void setId(Long id);
}
