package br.com.softdesign.model;

import br.com.softdesign.enums.VotoEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

public class StatusCooperado{

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StatusCooperado)) return false;
        StatusCooperado that = (StatusCooperado) o;
        return getStatus().equals(that.getStatus());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getStatus());
    }
}
