package br.com.softdesign.model;

import br.com.softdesign.enums.ResultadoPautaEnum;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "TB_PAUTA")

public class Pauta implements Identifiable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATA_CRIACAO")
    private Date dataCriacao;

    @NotNull(message = "O título não pode ser nulo")
    @NotBlank(message = "O título não pode conter um valor vazio")
    @Column(name = "TITULO", nullable = false, columnDefinition = "text")
    private String titulo;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TIMESTAMP_INICIO_SESSAO")
    private Date inicioSessao;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TIMESTAMP_FINAL_SESSAO")
    private Date finalSessao;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Column(name = "RESULTADO")
    @Enumerated(value = EnumType.STRING)
    private ResultadoPautaEnum resultado;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Column(name = "SIM_CONTABILIZADO")
    private Long simContabilizado;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @Column(name = "NAO_CONTABILIZADO")
    private Long naoContabilizado;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @OneToMany(mappedBy = "pauta", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Voto> votos = new ArrayList<>();

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public Date getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Date getInicioSessao() {
        return inicioSessao;
    }

    public void setInicioSessao(Date inicioSessao) {
        this.inicioSessao = inicioSessao;
    }

    public Date getFinalSessao() {
        return finalSessao;
    }

    public void setFinalSessao(Date finalSessao) {
        this.finalSessao = finalSessao;
    }

    public ResultadoPautaEnum getResultado() {
        return resultado;
    }

    public void setResultado(ResultadoPautaEnum resultado) {
        this.resultado = resultado;
    }

    public Long getSimContabilizado() {
        return simContabilizado;
    }

    public void setSimContabilizado(Long simContabilizado) {
        this.simContabilizado = simContabilizado;
    }

    public Long getNaoContabilizado() {
        return naoContabilizado;
    }

    public void setNaoContabilizado(Long naoContabilizado) {
        this.naoContabilizado = naoContabilizado;
    }

    public List<Voto> getVotos() {
        return votos;
    }

    public void setVotos(List<Voto> votos) {
        this.votos = votos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pauta)) return false;
        Pauta pauta = (Pauta) o;
        return getId().equals(pauta.getId()) &&
                getDataCriacao().equals(pauta.getDataCriacao()) &&
                getTitulo().equals(pauta.getTitulo()) &&
                getInicioSessao().equals(pauta.getInicioSessao()) &&
                getFinalSessao().equals(pauta.getFinalSessao()) &&
                getResultado() == pauta.getResultado() &&
                getSimContabilizado().equals(pauta.getSimContabilizado()) &&
                getNaoContabilizado().equals(pauta.getNaoContabilizado()) &&
                getVotos().equals(pauta.getVotos());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getDataCriacao(), getTitulo(), getInicioSessao(), getFinalSessao(), getResultado(), getSimContabilizado(), getNaoContabilizado(), getVotos());
    }

    @Override
    public String toString() {
        return "Pauta{" +
                "id=" + id +
                ", dataCriacao=" + dataCriacao +
                ", titulo='" + titulo + '\'' +
                ", inicioSessao=" + inicioSessao +
                ", finalSessao=" + finalSessao +
                ", resultado=" + resultado +
                ", simContabilizado=" + simContabilizado +
                ", naoContabilizado=" + naoContabilizado +
                ", votos=" + votos +
                '}';
    }
}
