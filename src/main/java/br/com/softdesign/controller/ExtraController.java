package br.com.softdesign.controller;

import br.com.softdesign.message.MessageSender;
import br.com.softdesign.model.StatusCooperado;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@RestController 
@RequestMapping({ "/extra" })
@CrossOrigin(value = "*")
public class ExtraController{

	private static final Logger LOG = LoggerFactory.getLogger(ExtraController.class);

	@Autowired
	private MessageSender sender;

	@Value("${softdesign.api.externa.consulta.cooperado}")
	private String apiExternaConsutlaCooperado;

	/**
	 * Endpoint que permite o acesso a uma API externa para consultar se um determinado CPF está
	 * habilitado ou não para exercer seu voto.
	 * @param cpf - CPF consultado.
	 * @return - ["ABLE_TO_VOTE", "UNABLE_TO_VOTE"] caso o cpf seja válido.
	 */
	@GetMapping(value="/status-cooperado/{cpf}")
	public ResponseEntity cooperadoStatus(@PathVariable(name="cpf", required = true) final String cpf){

		RestTemplate restTemplate = new RestTemplate();
		try{
			StatusCooperado result = restTemplate.getForObject(this.apiExternaConsutlaCooperado + cpf, StatusCooperado.class);
			return new ResponseEntity<>(result.getStatus(), HttpStatus.OK);
		}catch(RestClientException e){
			LOG.error(e.getMessage());
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

	}

}
