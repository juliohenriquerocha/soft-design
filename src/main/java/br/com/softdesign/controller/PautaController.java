package br.com.softdesign.controller;

import br.com.softdesign.exception.SoftDesignBusinessException;
import br.com.softdesign.exception.SoftDesignPersistenceException;
import br.com.softdesign.model.Pauta;
import br.com.softdesign.service.PautaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController 
@RequestMapping({ "/pauta" })
@CrossOrigin(value = "*")
public class PautaController extends GenericController<Pauta> {

	private static final Logger LOG = LoggerFactory.getLogger(PautaController.class);

	@Autowired
	private PautaService pautaService;

	@Override
	public PautaService getService() {
		return pautaService;
	}

	/**
	 * Endpoint para abertura de sessão de votação.
	 * @param id - id da Pauta
	 * @param minutos - número de minutos que a sessão ficará aberta. Default é 1
	 * @return - Response com status OK.
	 */
	@PostMapping( value = "/{id}/abrir-sessao")
	public ResponseEntity openSession(@PathVariable(name="id") final Long id,
									  @RequestParam(name = "minutos", required = false, defaultValue = "1") final Integer minutos) {
		try {
			getService().openSession(id, minutos);
			return new ResponseEntity(HttpStatus.OK);
		} catch (SoftDesignBusinessException | SoftDesignPersistenceException e) {
			LOG.error(e.getMessage());
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_ACCEPTABLE);
		}catch (Exception err){
			LOG.error(err.getMessage());
			return new ResponseEntity<>(err.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Endpoint para receber o voto de um Cooperado em uma determinada Pauta.
	 * @param id - id da Pauta
	 * @param idCooperado - id do Cooperado
	 * @param voto - voto do Cooperado ["Sim", "Não"]
	 * @return Response com status OK, caso o voto tenha sido computado corretamente.
	 */
	@PostMapping( value = "/{id}/votar")
	public ResponseEntity vote(@PathVariable(name="id") final Long id,
							   @RequestParam(name = "cooperado", required = true) final Long idCooperado,
							   @RequestParam(name = "voto", required = true) final String voto) {
		try {
			getService().vote(id, idCooperado, voto);
			return new ResponseEntity(HttpStatus.OK);
		} catch (SoftDesignBusinessException | SoftDesignPersistenceException e) {
			LOG.error(e.getMessage());
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_ACCEPTABLE);
		}catch (Exception err){
			err.printStackTrace();
			LOG.error(err.getMessage());
			return new ResponseEntity<>(err.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Endpoint para a contabilização dos votos em determinada Pauta.
	 * @param id - id da Pauta
	 * @return - a Pauta solicitada com sua respectiva apuração.
	 */
	@GetMapping(value="/{id}/contabilizar-resultado")
	public ResponseEntity countResult(@PathVariable(name="id") final Long id){
		try {
			Pauta pauta = getService().countResult(id);
			return new ResponseEntity(pauta, HttpStatus.OK);
		} catch (SoftDesignBusinessException | SoftDesignPersistenceException e) {
			LOG.error(e.getMessage());
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_ACCEPTABLE);
		}catch (Exception err){
			err.printStackTrace();
			LOG.error(err.getMessage());
			return new ResponseEntity<>(err.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}


}
