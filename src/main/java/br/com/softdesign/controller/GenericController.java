package br.com.softdesign.controller;

import br.com.softdesign.exception.SoftDesignBusinessException;
import br.com.softdesign.exception.SoftDesignPersistenceException;
import br.com.softdesign.model.Identifiable;
import br.com.softdesign.service.GenericService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * Classe abstrata com endpoints comuns para todas as entidades manipuladas na aplicação.
 * @author julio
 * @param <T extends Identifiable>
 */
public abstract class GenericController<T extends Identifiable> {

	private static final Logger LOG = LoggerFactory.getLogger(GenericController.class);

	public abstract GenericService<T> getService();

	/**
	 * Endpoint para persistência de um novo registro.
	 * @param object - o objeto a ser persistido.
	 * @return Response com o novo objeto persistido e o status OK.
	 */
	@RequestMapping(produces = "application/json", method = {RequestMethod.POST, RequestMethod.PUT})
	public ResponseEntity save(@Valid @RequestBody final T object){
		try {
			getService().save(object);
			return new ResponseEntity<>(object, HttpStatus.OK);
		} catch (SoftDesignBusinessException | SoftDesignPersistenceException e) {
			LOG.error(e.getMessage());
			return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_ACCEPTABLE);
		} catch (Exception e) {
			LOG.error(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}



	/**
	 * Endpoint que permite buscar um objeto pelo seu identificador
	 * @param id - id do objeto buscado
	 * @return Response com o objeto encontrado.
	 */
	@GetMapping(value = "/{id}")
	public ResponseEntity<T> findById(@PathVariable("id") Long id){
		try {
			T object = getService().findById(id);
			return new ResponseEntity<>(object, HttpStatus.OK);
		} catch (Exception e) {
			LOG.error(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Endpoint que permite a listagem de todos os objetos armazenados
	 * @return Response com a listagem de todos os objetos e o status OK.
	 */
	@GetMapping()
	public ResponseEntity findAll(){
		try {
			return new ResponseEntity<>(getService().findAll(), HttpStatus.OK);
		} catch (Exception e) {
			LOG.error(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}




}
