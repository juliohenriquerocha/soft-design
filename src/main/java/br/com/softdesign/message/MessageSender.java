package br.com.softdesign.message;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MessageSender {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendMessage(String message, Long delay) {

        rabbitTemplate.convertAndSend("spring-boot-exchange", "pauta", message, m -> {
                m.getMessageProperties().setHeader("x-delay", delay != null ? delay : 0);
                m.getMessageProperties().setDelay(delay != null ? delay.intValue() : 0);
                return m;
        });
    }
}
