package br.com.softdesign.message;

import java.util.concurrent.CountDownLatch;
import org.springframework.stereotype.Component;

@Component
public class Receiver {

    private CountDownLatch latch = new CountDownLatch(1);

    /**
     * Método que printa as mensagens recebidas na mensageria.
     * Utilizada apenas para fins de testes.
     * @param message - mensagem recebida.
     */
    public void receiveMessage(String message) {
        System.out.println("Received <" + message + ">");
        latch.countDown();
    }

    public CountDownLatch getLatch() {
        return latch;
    }

}